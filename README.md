***

# TODO
This guide is work in progress, and at this point, the following needs to be addressed:

- Add section about nullable keywords (`nullable`, `nonnull`, etc...)
- Add section about generics (`NSArray <NSString *> *`)
- Fill out Naming Conventions -> Properties/Variables
- Fill out Naming Conventions -> Constants
- Fill out Naming Conventions -> Enumerated Typed

***


# The official Mobile Identity Objective-C coding conventions.

This guide outlines the coding conventions used at [Mobile Identity ApS](http://www.mobile-identity.com). The guide is originally a fork from the [Ray Wenderlich Objective-C style guide](https://github.com/raywenderlich/objective-c-style-guide). Feel free to open any PR with changes you want to propose.

The guide is divided into 3 sections:

* [Style Guide](#style-guide)
* [Naming Conventions](#naming-conventions)
* [Best Practices](#best-practices)

**Style Guide:** Guidelines relating to code organization, structure, formatting and indentation etc., which **must** be conformed to by all developers when writing new code. The purpose of these guidelines is to ensure a general level of code hygiene and consistency within and across projects. 

**Naming Conventions:** A series of naming conventions for methods, classes, protocols, variables etc. largely based on Apple's [Coding Guidelines for Cocoa](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html). The purpose is to help ensure that code is expressive, readable and easy to understand by any developer working on a project.

**Best Practices** A collection of architectural patterns, styles and approaches to development, outlining preferred practices at Mobile Identity. These are examples of good ways to solve problems/structure code that we agree on and should be adopted as often as it makes sense.

The guide should be used whenever creating new classes. Changes within an existing class that follows a different style guide, should be done by either following that style guide or by completely updating the class to conform to this guide.

Here are some of the documents from Apple that informed the guide. If something isn't mentioned here, it's probably covered in great detail in one of these:

* [The Objective-C Programming Language](http://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/ObjectiveC/Introduction/introObjectiveC.html)
* [Cocoa Fundamentals Guide](https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CocoaFundamentals/Introduction/Introduction.html)
* [Coding Guidelines for Cocoa](https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html)
* [iOS App Programming Guide](http://developer.apple.com/library/ios/#documentation/iphone/conceptual/iphoneosprogrammingguide/Introduction/Introduction.html)

# Style Guide

* [Language](#language)
* [Code Organization](#code-organization)
* [Spacing](#spacing)
* [Line Breaks](#line-breaks)
* [Braces](#braces)
* [Init Methods](#init-methods)
* [Class Constructor Methods](#class-constructor-methods)
* [Singletons](#singletons)
* [Properties](#properties)
* [Literals](#literals)
* [Constants](#constants)
* [Enumerated Types](#enumerated-types)
* [Case Statements](#case-statements)
* [Booleans](#booleans)
* [Conditionals](#conditionals)
* [CGRect Functions](#cgrect-functions)
* [Error handling](#error-handling)
* [Documentation](#documentation)
* [Helpers](#helpers)

## Language

US English must be used.

**Preferred:**

```
UIColor *myColor = [UIColor whiteColor];
```

**Not Preferred:**

```
UIColor *myColour = [UIColor whiteColor];
```

## Code Organization

Use `#pragma mark -` to categorize methods in functional groupings and protocol/delegate implementations following this general structure:

```
#pragma mark - Lifecycle

- (instancetype)init {}
- (void)dealloc {}
- (void)viewDidLoad {}
- (void)viewWillAppear:(BOOL)animated {}
- (void)didReceiveMemoryWarning {}


#pragma mark - Custom Accessors

- (void)setCustomProperty:(id)value {}
- (id)customProperty {}


#pragma mark - IBActions

- (IBAction)submitData:(id)sender {}


#pragma mark - Public

- (void)publicMethod {}


#pragma mark - Private

- (void)privateMethod {}


#pragma mark - Protocol conformance
#pragma mark - UITextFieldDelegate
#pragma mark - UITableViewDataSource
#pragma mark - UITableViewDelegate


#pragma mark - NSCopying

- (id)copyWithZone:(NSZone *)zone {}


#pragma mark - NSObject

- (NSString *)description {}
```

## Spacing

* Indent using 4 spaces. Never indent with tabs. Be sure to set this preference in Xcode.
* There should be exactly one blank line between methods to aid in visual clarity and organization. 
* Keep two blank lines above a `#pragma mark -` to clearly separate sections of code
* In method signatures, keep a space after the method type (-/+ symbol) and a space before asterisks.

**Preferred:**

```
- (void)setExampleText:(NSString *)text image:(UIImage *)image;
```

**Not Preferred:**

```
-(void)setExampleText:(NSString*)text image:(UIImage*)image;
```


## Braces

Method braces and other braces (`if`/`else`/`switch`/`while` etc.) always open on the same line as the statement but close on a new line.

**Preferred:**

```
if (user.isHappy) {
	// Do something
} else {
    // Do something else
}
```

**Not Preferred:**

```
if (user.isHappy)
{
	// Do something
}
else 
{
	// Do something else
}
```

## Init Methods

Init methods must have a return type of `instancetype` instead of `id` and must follow the principle of golden path. 

```
- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}
```

## Class Constructor Methods

As with init methods, where class constructor methods are used, these should always return a type of `instancetype` and never `id`. This ensures the compiler correctly infers the result type. More information on `instancetype` can be found on [NSHipster.com](http://nshipster.com/instancetype/).

```
@interface Airplane

+ (instancetype)airplaneWithType:(MIAirplaneType)type;

@end
```

## Singletons

Singleton objects should use a thread-safe pattern for creating their shared instance.

```
+ (instancetype)sharedInstance {
    static id sharedInstance = nil;

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}
```

This will prevent [possible and sometimes prolific crashes](http://cocoasamurai.blogspot.com/2011/04/singletons-your-doing-them-wrong.html).

## Properties

Use properties over instance variables unless there is a clear improvement in end user experience for not using properties. When using properties, instance variables must always be accessed and mutated using `self.`. This means that all properties will be visually distinct, as they will all be prefaced with `self.` Also, to avoid confusion, local variables should never contain underscores.

**Preferred:**

```
self.someProperty = @"Some value";
NSString *someLocalVariable;
```

**Not Preferred:**

```
_someValue = @"Some value";
NSString *_localVariable
```

Exceptions to this are inside initializers and within custom setters and getters. Here the backing instance variable (i.e. `_variableName`) must be used directly to avoid any potential side effects of the getters/setters. For more information on using Accessor Methods in Initializer Methods and dealloc, see [here](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmPractical.html#//apple_ref/doc/uid/TP40004447-SW6).

Property attributes must be explicitly listed, and will help new programmers when reading the code. The order of properties should be atomicity, storage and then access permissions.

Asterisks indicating pointers belong with the variable, e.g., `NSString *text` not `NSString* text` or `NSString * text`, except in the case of constants.

**Preferred:**

```
@property (nonatomic, weak, readonly) IBOutlet UIView *containerView;
@property (nonatomic, strong, readwrite) NSString *tutorialName;
```

**Not Preferred:**

```
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (nonatomic) NSString* tutorialName;
```

## Literals

`NSString`, `NSDictionary`, `NSArray`, and `NSNumber` literals must be used whenever creating immutable instances of those objects. Pay special care that `nil` values can not be passed into `NSArray` and `NSDictionary` literals, as this will cause a crash.

**Preferred:**

```
NSArray *names = @[@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul"];
NSDictionary *productManagers = @{@"iPhone": @"Kate", @"iPad": @"Kamal", @"Mobile Web": @"Bill"};
NSNumber *shouldUseLiterals = @YES;
NSNumber *buildingStreetNumber = @10018;
```

**Not Preferred:**

```
NSArray *names = [NSArray arrayWithObjects:@"Brian", @"Matt", @"Chris", @"Alex", @"Steve", @"Paul", nil];
NSDictionary *productManagers = [NSDictionary dictionaryWithObjectsAndKeys: @"Kate", @"iPhone", @"Kamal", @"iPad", @"Bill", @"Mobile Web", nil];
NSNumber *shouldUseLiterals = [NSNumber numberWithBool:YES];
NSNumber *buildingStreetNumber = [NSNumber numberWithInteger:10018];
```

## Constants

Use constants over in-line string literals or numbers to centralize information and allow for easy reproduction of commonly used values. Constants must be declared as `static` constants and not `#define`s unless explicitly being used as a macro. If a constant should be visible outside the scope of the implementation (e.g. for testing), then `extern` should be used when defining the constant in the header file.

**Preferred:**

```
static NSString * const MIAboutViewControllerCompanyName = @"Mobile Identity";

static CGFloat const MIImageThumbnailHeight = 50.0;
```

and

```
// SomeClass.h
extern NSString * const MIAboutViewControllerCompanyName;

// SomeClass.m
NSString * const MIAboutViewControllerCompanyName = @"Mobile Identity";
```

**Not Preferred:**

```
#define CompanyName @"Mobile Identity"

#define thumbnailHeight 2
```

## Enumerated Types

When using `enum`s, it is recommended to use the new fixed underlying type specification because it has stronger type checking and code completion. The SDK now includes a macro to facilitate and encourage use of fixed underlying types: `NS_ENUM()`. If explicit value assignment is not used, then always make the first value in an enum be an "unknown" type, to make it explicit that this case should also be handled.

**For Example:**

```
typedef NS_ENUM(NSInteger, MILeftMenuTopItemType) {
    MILeftMenuTopItemUnknown,
    MILeftMenuTopItemMain,
    MILeftMenuTopItemShows,
    MILeftMenuTopItemSchedule
};
```

You can also make explicit value assignments (showing older k-style constant definition):

```
typedef NS_ENUM(NSInteger, MIGlobalConstants) {
    MIPinSizeMin = 1,
    MIPinSizeMax = 5,
    MIPinCountMin = 100,
    MIPinCountMax = 500,
};
```

Older k-style constant definitions should be **avoided** unless writing CoreFoundation C code (unlikely).

**Not Preferred:**

```
enum GlobalConstants {
    kMaxPinSize = 5,
    kMaxPinCount = 500,
};
```

## Case Statements

Braces are not required for case statements, unless enforced by the complier.

```
switch (condition) {
    case 1:
        // ...
        break;
    case 2: {
        // ...
        // Multi-line example using braces
        break;
    }
    case 3:
        // ...
        break;
    default: 
        // ...
        break;
}
```

There are times when the same code can be used for multiple cases, and a fall-through should be used. A fall-through is the removal of the 'break' statement for a case thus allowing the flow of execution to pass to the next case value.  A fall-through must be commented for coding clarity.

```
switch (condition) {
    case 1:
        // ** Fall-through! **
    case 2:
        // Code executed for values 1 and 2
        break;
    default: 
        // ...
        break;
}
```

When using an enumerated type for a switch, 'default' is not needed if the purpose is to cover all enum values. This is convenient, as the compiler will now warn you when adding a new value to the enum that is not handled. For example:

```
MILeftMenuTopItemType menuType = MILeftMenuTopItemMain;

switch (menuType) {
    case MILeftMenuTopItemMain:
        // ...
        break;
    case MILeftMenuTopItemShows:
        // ...
        break;
    case MILeftMenuTopItemSchedule:
        // ...
        break;
}
```

## Booleans

Objective-C uses `YES` and `NO`.  Therefore `true` and `false` must only be used for CoreFoundation, C or C++ code.  Since `nil` resolves to `NO` it is unnecessary to compare it in conditions. Never compare something directly to `YES`, because `YES` is defined to 1 and a `BOOL` can be up to 8 bits.

This allows for more consistency across files and greater visual clarity.

**Preferred:**

```
if (someObject) {}
if (![anotherObject boolValue]) {}
```

**Not Preferred:**

```
if (someObject == nil) {}
if ([anotherObject boolValue] == NO) {}
if (isAwesome == YES) {} // Never do this.
if (isAwesome == true) {} // Never do this.
```

## Conditionals

Conditional bodies must always use braces even when a conditional body could be written without braces (e.g., it is one line only) to prevent errors. These errors include adding a second line and expecting it to be part of the if-statement. Another, [even more dangerous defect](http://programmers.stackexchange.com/a/16530) may happen where the line "inside" the if-statement is commented out, and the next line unwittingly becomes part of the if-statement. In addition, this style is more consistent with all other conditionals, and therefore more easily scannable.

**Preferred:**

```
if (!error) {
    return success;
}
```

**Not Preferred:**

```
if (!error)
    return success;
```

or

```
if (!error) return success;
```

## CGRect Functions

When accessing the `x`, `y`, `width`, or `height` of a `CGRect`, always use the [`CGGeometry` functions](http://developer.apple.com/library/ios/#documentation/graphicsimaging/reference/CGGeometry/Reference/reference.html) instead of direct struct member access. From Apple's `CGGeometry` reference:

> All functions described in this reference that take CGRect data structures as inputs implicitly standardize those rectangles before calculating their results. For this reason, your applications should avoid directly reading and writing the data stored in the CGRect data structure. Instead, use the functions described here to manipulate rectangles and to retrieve their characteristics.

**Preferred:**

```
CGRect frame = self.view.frame;

CGFloat x = CGRectGetMinX(frame);
CGFloat y = CGRectGetMinY(frame);
CGFloat width = CGRectGetWidth(frame);
CGFloat height = CGRectGetHeight(frame);
CGRect frame = CGRectMake(0.0, 0.0, width, height);
```

**Not Preferred:**

```
CGRect frame = self.view.frame;

CGFloat x = frame.origin.x;
CGFloat y = frame.origin.y;
CGFloat width = frame.size.width;
CGFloat height = frame.size.height;
CGRect frame = (CGRect){ .origin = CGPointZero, .size = frame.size };
```

## Error handling

When methods return an error parameter by reference, switch on the returned value, not the error variable.

**Preferred:**

```
NSError *error;
if (![self trySomethingWithError:&error]) {
    // Handle Error
}
```

**Not Preferred:**

```
NSError *error;
[self trySomethingWithError:&error];
if (error) {
    // Handle Error
}
```

Some of Apple’s APIs write garbage values to the error parameter (if non-NULL) in successful cases, so switching on the error can cause false negatives (and subsequently crash).

## Documentation

Documentation must conform to the format of [HeaderDoc](https://developer.apple.com/library/mac/documentation/DeveloperTools/Conceptual/HeaderDoc/tags/tags.html). Keep documentation consistent and readable:

* Document classes with a `@brief` above the interface declaration
* Document properties with only a `@brief`
* Document methods with a `@brief` as well as parameters (`@param`) and return (`@return`) values
* Add a blank line between `@brief`, `@param` and `@return` sections
* Keep `@param`s grouped as a section

See the [Helpers](#helpers) section for plugins to help auto-generate this documentation.

**Preferred**

```
/**
 *  @brief The types of genders for an `MIUser`.
 */
typedef NS_ENUM(NSInteger, MIUserGenderType) {
    /** The gender is unknown. */
    MIGenderUnknown
	/** A male. */
    MIGenderMale,
    /** A female. */
    MIGenderFemale,
};

/**
 *  @brief The Mobile Identity user object which is used for logging in to the app.
 */
@interface MIUser : NSObject

/**
 *  @brief The full name of the user.
 */
@property (nonatomic, strong, readonly) NSString *name;

/**
 *  @brief Returns an array of `MIUser`s that has the same gender as specified.
 *
 *  @param gender	The gender to use when filtering friends.
 *  @param age 	 	The age to use when filtering friends.
 *
 *  @return A list of friends with the same gender as specified.
 */
- (NSArray *)friendsOfGender:(MIGender)gender withAge:(NSNumber *)age;
```

**Not Preferred**

```
/// The types of genders for an `MIUser`.
typedef NS_ENUM(NSInteger, MIUserGenderType) {
    /**
     *  @brief The types of genders for an `MIUser`.
     */
    MIGenderUnknown
};

///an interface.
@interface MIUser : NSObject

/**
 *  @brief Returns an array of `MIUser`s that has the same gender as specified.
 *  @param gender The gender to use when filtering friends.
 *  @return A list of friends with the same gender as specified.
 */
- (NSArray *)friendsOfGender:(MIGender)gender;
```

## Helpers

This section contains a list of useful Xcode plugins that can ease the task of conforming to this code style. [Alcatraz](http://alcatraz.io) can be used for managing your plugins.

### [VVDocumenter-Xcode](https://github.com/onevcat/VVDocumenter-Xcode)

VVDocumenter-Xcode helps auto-generating the needed documentation for property and method declarations. Use the following settings for the plugin:

- [x] Use spaces instad of tabs (2 spaces)
- [ ] Use @since to all comments
- [x] Use @brief for description
- [ ] Use /\*! (HeaderDoc style) instad of /**
- [x] Add blank lines between sections
- [x] Align argument comments
- [ ] Add Default User Information (objc only)
- [ ] Prefix each comment line iwth whitespace only
- [x] Prefix each comment line with a star (objc only)
- [ ] Prefix each comment line with three slashes

# Naming Conventions

This section describes the naming conventions, which must be used when adding new classes, methods, properties etc. It is based largely on Apple's [Coding Guidelines for Cocoa](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CodingGuidelines/CodingGuidelines.html), which addresses this in great detail.

* [General]()
* [Classes]()
* [Protocols]()
* [Categories]()
* [Methods]()
* [Properties/Variables]()
* [Constants]()
* [Enumerated Types]()

## General

In general **Cocoa** uses an explicit and expressive naming scheme, preferring full words to abbreviations. Names should communicate the purpose or intent of the named thing. A name should be as clear and brief as possible, but clarity must not suffer because of brevity. For names composed of multiple words the first letter of each word must be capitalized, also known as *camel-casing* ([Cocoa Code Naming Basics](https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/CodingGuidelines/Articles/NamingBasics.html)):

**Preferred:**

```
- (void)insertObject:(id)object atIndex:(NSInteger)index;
```
```
@property (nonatomic, strong) UIButton *submitButton;
```
```
int customerId;
```

**Not preferred:**

```
- (void)insert:(id)object at:(NSInteger)index;
```
```
@property (nonatomic, strong) UIButton *submitbtn;
```
```
int cid;
```

## Classes

Class names must start with a 3-letter uppercase namespace prefix, specifying the scope of the class followed by a capitalized first letter and *camel casing* the rest of the name. If this is a project specific class, use a project specific namespace prefix. However if you think the class could be generalized and used across projects, consider using the Mobile Identity (**MI**) prefix instead. Class names should contain a noun that clearly expresses what the class represents. 

**Preferred:**

```
- MIPushHandler
- XYZContactModel
```

**Not preferred:**

```
- MIpushHandler
- ContactModel
```

## Protocols

Protocols should include a namespace prefix and be named such as not to be confused with classes. Using the "...ing" or "...able" form of a verb are good ways to indicate that this is not a class. If the protocol has only one relevant implementation the "...Protocol" suffix can be used to link the protocol with its *principal expression*.

**Preferred:**

```
- MISerializable
- XYZModelParsing
- XYZStorageProtocol
```

**Not preferred:**

```
- MISerializer
- XYZModelParsingProtocol
- Archiving
```

## Categories

Categories can be used to group similar functionality and should be named to reflect this functionality. Method names defined by categories must start with a 3-letter lower case namespace prefix followed by an underscore. This is to clearly differentiate what is part of a category from that, which is part of the class. As with classes and protocols the namespace prefix should depend on whether the category is part of the project or one, which could be applied for general use.

**Preferred:**

```
@interface NSObject (MISerialization)
- (id)unw_serializeWithMapping:(NSDictionary *)mapping
```
**Not preferred:**

```
@interface NSObject (Serialization)
- (id)serializeWithMapping:(NSDictionary *)mapping
```

## Methods

In general method names should start with a lower case letter and then capitalize the first letter of every following word, i.e. *camel casing*. An exception to this rule is methods, which refer to well-known upper case acronyms. For example:

```
- (NSString *)PINCode;
- (NSString *)ISBNForBookID:(NSNumber *)bookID;
```

For methods that represent an action an object takes, start the name with a verb: 

```
- (void)invokeWithTarget:(id)target;
- (void)startRecording;
```

Use keywords before each argument of a method to describe them, but don't use "and" to link keywords that are attributes of the receiver. Save the use of "and" for methods that perform two separate actions.

**Preferred:**

```
- (XYZProduct *)productWithID:(NSNumber *)productID fromList:(XYZList *)list;
- (void)addContactWithName(NSString *)name age:(NSNumber *)age email:(NSString *)email;
- (void)registerMessage:(XYZMessage *)message andNotifySender:(XYZSender *)sender;
```
**Not preferred:**

```
- (XYZProduct *)productFromList:(XYZList *)list withId:(NSNumber *)productId;
- (void)addContactWithName(NSString *)name andAge:(NSNumber *)age andEmail:(NSString *)email;
```

## Properties/Variables

Variables should be named as descriptively as possible. Single letter variable names should be avoided except in `for()` loops.

## Constants
**TODO**
## Enumerated Types
**TODO**

# Best Practices

This section is reserved for agreed best practices at Mobile Identity. Consider these guidelines rather than imperatives when writing code. These practices should however be adopted whenever they make sense to ensure high code quality across projects.

* [Code Commenting]()
* [Using instance variables]()
* [Properties]()
* [Booleans]()
* [Ternary Operator]()
* [Gold Path]()
* [ReactiveCocoa]()
* [Xcode Projects]()

## Code Commenting

When they are needed and add value, comments should be used to explain **why** a particular piece of code does something. Any comments that are used must be kept up-to-date or deleted.

Block comments should generally be avoided, as code should be as self-documenting as possible, with only the need for intermittent, few-line explanations.

*Exception: This does not apply to those comments used to generate documentation. Header files should always be documented*


## Using instance variables

Unless there are clear consequences for performance critical code etc. prefer properties to instance variables. There will always be a performance hit when accessing ivars through properties, however that performance hit should be considered relative to the use case when ever considering dropping properties for performance reasons.

Private properties should be used in place of instance variables whenever possible. Although using instance variables is a valid way of doing things, by agreeing to prefer properties our code will be more consistent. 

## Properties

Properties declared in a headerfile should be tagged `readonly` by default to restrict access control. Whenever exposing a property, carefully consider if it is meant to be manipulated directly by a consumer of the class. If not, make it `readonly` to ensure this. Properties declared as `readonly` in the header file, can be changed to allow internal altering, by re-declaring them as `readwrite` in the implementation file.

Private properties should be declared in class extensions (anonymous categories) in the implementation file of a class. Named categories (such as `MIPrivate` or `private`) should never be used unless extending another class. The anonymous category can be shared/exposed for i.e. testing using the `_Private.h` file naming convention.

**For Example:**

```
@interface MIDetailViewController ()

@property (nonatomic, strong, readwrite) GADBannerView *googleAdView;
@property (nonatomic, strong, readwrite) ADBannerView *iAdView;
@property (nonatomic, strong, readwrite) UIWebView *adXWebView;

@end
```

Properties with mutable counterparts (e.g. `NSString`) should prefer `copy` instead of `strong` to ensure that the property is indeed immutable. Even if you declared a property as `NSString` somebody might pass in an instance of an `NSMutableString` and then change it without you noticing that.  

**Preferred:**

```
@property (nonatomic, copy, readonly) NSString *tutorialName;
```

**Not Preferred:**

```
@property (nonatomic, strong, readonly) NSString *tutorialName;
```

## Booleans

If the name of a `BOOL` property is expressed as an adjective, the property can omit the “is” prefix but specify the conventional name for the get accessor, for example:

```
@property (assign, getter=isEditable) BOOL editable;
```

Text and example taken from the [Cocoa Naming Guidelines](https://developer.apple.com/library/mac/#documentation/Cocoa/Conceptual/CodingGuidelines/Articles/NamingIvarsAndTypes.html#//apple_ref/doc/uid/20001284-BAJGIIJE).

## Ternary Operator

The Ternary operator, `?:` , should only be used when it increases clarity or code neatness. A single condition is usually all that should be evaluated. Evaluating multiple conditions is usually more understandable as an `if` statement, or refactored into instance variables. In general, the best use of the ternary operator is during assignment of a variable and deciding which value to use.

Non-boolean variables should be compared against something, and parentheses are added for improved readability.  If the variable being compared is a boolean type, then no parentheses are needed.

**Preferred:**

```
NSInteger value = 5;
result = (value != 0) ? x : y;

BOOL isHorizontal = YES;
result = isHorizontal ? x : y;
```

**Not Preferred:**

```
result = a > b ? x = c > d ? c : d : y;
```

## Golden Path

When coding with conditionals, the left hand margin of the code should be the "golden" or "happy" path.  That is, don't nest `if` statements.  Multiple return statements are OK.

**Preferred:**

```
- (void)someMethod {
    if (![someOther boolValue]) {
	    return;
    }

    // Do something important
}
```

**Not Preferred:**

```
- (void)someMethod {
    if ([someOther boolValue]) {
        // Do something important
    }
}
```

## Xcode projects

The physical files should be kept in sync with the Xcode project files in order to avoid file sprawl. Folders in the filesystem should reflect any Xcode groups created. Code should be grouped not only by type, but also by feature for greater clarity.